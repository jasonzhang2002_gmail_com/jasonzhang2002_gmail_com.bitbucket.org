var myApp = angular.module('elementink', [ 'ui.router', 'product' ]);

//show current language
myApp.controller("mainCtrl", function($scope, $location, $window){
	$scope.lang='zh';
	if ($location.search().lang)
	{
		$scope.lang=$location.search().lang;
	}

	$scope.setLang=function(lang){
		$window.location.href=$window.location.href+"?lang="+lang;
		$window.location.reload();
		//$scope.lang=l;
		//$scope.$state.go(".");
	};
	
	$scope.$on('$stateChangeStart',  function(event, toState, toParams, fromState, fromParams){
		//remember what state we are gong to
		$scope.tostate=toState.name;
	});
	
	
	
	
	
});

//remember current scope
myApp.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
});



myApp.config(function($stateProvider, $urlRouterProvider) {
	

	
	//calculate the url
	function localizedTemplate(stateParams)
	{
		
		var statename=angular.element(document).scope().tostate;
		var lang=angular.element(document).scope().lang;
		var file=statename.replace('.', "/");
		
		var partnum=file.split("/").length;
		if (partnum==1)
		{
			if (statename=='index' || statename=="")
			{
				file="indexcontent";
			} else
			{
				//not used
				file=statename+"/nav";
			}
		} else
		{
			if (lang=='zh')
			{
				file+="_zh";
			}
		}
		
		
		file+=".html";
		console.log("state name :"+statename+". file:"+file);
		return file;
	}
	
	var sections=['about', 'contactus', 'marketing', 'news', 'product', 'services'];
	$urlRouterProvider.otherwise("/index");
	for (var i=0; i<sections.length; i++)
	{
		$urlRouterProvider.when(sections[i], sections[i]+"/index");
	}
	//
	
	$stateProvider.state('index', {
		url : "/index",
		templateUrl : "indexcontent.html"
	});
	
	//section index
	for (var i=0; i<sections.length; i++)
	{
		$stateProvider.state(sections[i], {
			url : "/"+sections[i],
			abstract:true,
			//has to be static url
			templateUrl : sections[i]+"/nav.html"
		}).state(sections[i]+".index", {
			url:"/index",
			templateUrl:localizedTemplate
		});
		
	}
	
	//about section.
	$stateProvider.state('about.cert', {
		url : "/cert",
		templateUrl :localizedTemplate
	}).state('about.factory', {
		url : "/factory",
		templateUrl : localizedTemplate
	}).state('about.staff', {
		url : "/staff",
		templateUrl : localizedTemplate
	});
});
