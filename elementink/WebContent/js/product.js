var product = angular.module('product', []);
product.controller('productCtrl', function($scope, $http) {
	$http.get('data/products.json').success(function(data) {
		$scope.products = data;
		
		
		var pchunks=[];
		for(var i=0; i<data.length; i+=4)
		{
			var chunk=[];
			for (var j=0; j<4; j++)
			{
				if (i+j<data.length)
				{
					chunk.push(data[i+j]);
				}
			}
			if (chunk.length>0)
			{
				pchunks.push(chunk);
			}
		}
		$scope.pchunks=pchunks;
		
	});

	$scope.startCarosel = function(id) {
		$(id).carousel();
	};
});
//http://blog.brunoscopelliti.com/run-a-directive-after-the-dom-has-finished-rendering
product.directive("productCarousel", ["$timeout", function($timeout) {
	
	return {
        link: function (scope, elem, attrs, ctrl) {
            var startCarousel = function () {                
               $(elem).carousel();
            };
            // hello();
            $timeout(startCarousel, 0);
            // It works even with a delay of 0s
        }
    } ;  
}]);
